# Protocol Examples

## Basic

HLP

## File Manipulation

### UP

Example: `UP:test.txt:some test data`

### DOWN

Example: `DOWN:blah.sh`

## Information

### SHOW IFACE

Sample output:

```
eth0      Link encap:Ethernet  HWaddr 00:24:e8:cf:31:aa
          inet addr:131.94.130.30  Bcast:131.94.130.255  Mask:255.255.255.0
          inet6 addr: fe80::224:e8ff:fecf:31aa/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:22112205 errors:0 dropped:0 overruns:0 frame:0
          TX packets:834167 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:2702425732 (2.7 GB)  TX bytes:679383689 (679.3 MB)

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:2074511 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2074511 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:152129514 (152.1 MB)  TX bytes:152129514 (152.1 MB)

wlan0     Link encap:Ethernet  HWaddr 00:26:5e:39:6f:41
          inet addr:172.25.3.97  Bcast:172.25.3.255  Mask:255.255.254.0
          inet6 addr: fe80::226:5eff:fe39:6f41/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:745244 errors:0 dropped:0 overruns:0 frame:0
          TX packets:33342 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:200163708 (200.1 MB)  TX bytes:6405434 (6.4 MB)
```

### SHOW NET

Sample output:

```
-- ROUTING INFO --
Active Internet connections (w/o servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State
tcp        0      0 localhost:60748         localhost:27017         ESTABLISHED
tcp        0     80 ubi-tan.cs.fiu.edu:ssh  c-73-179-0-9.hsd1:11616 ESTABLISHED
tcp        0      0 localhost:27017         localhost:60748         ESTABLISHED
tcp        0      0 localhost:48690         localhost:9999          ESTABLISHED
tcp        0      0 localhost:27017         localhost:60744         ESTABLISHED
tcp        0      0 localhost:9999          localhost:48690         ESTABLISHED
tcp        0      0 localhost:9999          localhost:48688         TIME_WAIT
tcp        0     64 ubi-tan.cs.fiu.edu:ssh  c-73-179-0-9.hsd1:12480 ESTABLISHED
tcp        0      0 localhost:60746         localhost:27017         ESTABLISHED
tcp        0      0 localhost:27017         localhost:60747         ESTABLISHED
tcp        0      1 ubi-tan.cs.fiu.ed:44472 209.85.202.26:smtp      SYN_SENT
tcp        0      0 localhost:27017         localhost:60746         ESTABLISHED
tcp        0      0 localhost:60747         localhost:27017         ESTABLISHED
tcp        0      0 localhost:60745         localhost:27017         ESTABLISHED
tcp        0      0 localhost:27017         localhost:60745         ESTABLISHED
tcp        0      0 localhost:60744         localhost:27017         ESTABLISHED
Active UNIX domain sockets (w/o servers)
Proto RefCnt Flags       Type       State         I-Node   Path
unix  2      [ ]         DGRAM                    12696    /var/lib/samba/private/msg.sock/712
unix  2      [ ]         DGRAM                    13657    /var/lib/samba/private/msg.sock/726
unix  2      [ ]         DGRAM                    13254    /var/run/wpa_supplicant/wlan0
unix  26     [ ]         DGRAM                    10199    /dev/log
unix  2      [ ]         DGRAM                    12683    /var/lib/samba/private/msg.sock/667
unix  2      [ ]         DGRAM                    15030    /var/lib/samba/private/msg.sock/1786
unix  2      [ ]         DGRAM                    1245816
unix  2      [ ]         DGRAM                    13677
unix  3      [ ]         STREAM     CONNECTED     1102148
unix  3      [ ]         STREAM     CONNECTED     1247529  @/tmp/.X11-unix/X0
unix  2      [ ]         DGRAM                    14752
unix  3      [ ]         STREAM     CONNECTED     1284590
unix  2      [ ]         DGRAM                    1102205
unix  3      [ ]         STREAM     CONNECTED     215885   @/com/ubuntu/upstart
unix  3      [ ]         STREAM     CONNECTED     13601    @/com/ubuntu/upstart
unix  3      [ ]         STREAM     CONNECTED     1102145
unix  3      [ ]         STREAM     CONNECTED     1285307  /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    1128268
unix  3      [ ]         STREAM     CONNECTED     13716    /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    1284566
unix  3      [ ]         STREAM     CONNECTED     1247565  @/tmp/.X11-unix/X0
unix  3      [ ]         STREAM     CONNECTED     14932    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1284628  /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    1276735
unix  3      [ ]         STREAM     CONNECTED     1246617  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1102152
unix  3      [ ]         STREAM     CONNECTED     1102149
unix  3      [ ]         STREAM     CONNECTED     1273755  /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    14940
unix  3      [ ]         STREAM     CONNECTED     1102130
unix  3      [ ]         STREAM     CONNECTED     1285369
unix  3      [ ]         STREAM     CONNECTED     10664    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1284621
unix  3      [ ]         STREAM     CONNECTED     1273741
unix  3      [ ]         STREAM     CONNECTED     10649    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1102174
unix  3      [ ]         STREAM     CONNECTED     1247582
unix  3      [ ]         STREAM     CONNECTED     60821
unix  3      [ ]         STREAM     CONNECTED     1102154
unix  3      [ ]         STREAM     CONNECTED     1102162
unix  3      [ ]         STREAM     CONNECTED     1247533  @/tmp/.X11-unix/X0
unix  3      [ ]         STREAM     CONNECTED     1102132
unix  3      [ ]         STREAM     CONNECTED     1248536  @/tmp/.X11-unix/X0
unix  3      [ ]         STREAM     CONNECTED     1102155
unix  3      [ ]         STREAM     CONNECTED     1102183
unix  3      [ ]         STREAM     CONNECTED     1102166
unix  3      [ ]         STREAM     CONNECTED     1102158
unix  3      [ ]         STREAM     CONNECTED     1102163
unix  3      [ ]         STREAM     CONNECTED     1102182
unix  3      [ ]         STREAM     CONNECTED     61582
unix  2      [ ]         DGRAM                    1097411
unix  3      [ ]         STREAM     CONNECTED     13007
unix  3      [ ]         STREAM     CONNECTED     1285401
unix  3      [ ]         STREAM     CONNECTED     1102178
unix  3      [ ]         STREAM     CONNECTED     1102175
unix  3      [ ]         STREAM     CONNECTED     1102159
unix  3      [ ]         STREAM     CONNECTED     1247600
unix  3      [ ]         STREAM     CONNECTED     1102133
unix  3      [ ]         STREAM     CONNECTED     13228
unix  3      [ ]         STREAM     CONNECTED     1102167
unix  3      [ ]         STREAM     CONNECTED     1102181
unix  3      [ ]         STREAM     CONNECTED     1102138
unix  3      [ ]         STREAM     CONNECTED     1246437  /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    12790
unix  3      [ ]         STREAM     CONNECTED     1102137
unix  3      [ ]         STREAM     CONNECTED     1247605  @/tmp/dbus-WpSqvN3hEl
unix  3      [ ]         STREAM     CONNECTED     1102139
unix  3      [ ]         STREAM     CONNECTED     1333812
unix  3      [ ]         STREAM     CONNECTED     1102141
unix  2      [ ]         DGRAM                    1333265
unix  3      [ ]         STREAM     CONNECTED     12743    /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    1333801
unix  3      [ ]         STREAM     CONNECTED     12794    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     11625    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1102144
unix  3      [ ]         STREAM     CONNECTED     12803    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1102134
unix  3      [ ]         STREAM     CONNECTED     10598
unix  3      [ ]         STREAM     CONNECTED     1248490  /var/run/acpid.socket
unix  3      [ ]         STREAM     CONNECTED     1102188
unix  3      [ ]         DGRAM                    214635
unix  2      [ ]         DGRAM                    1284664
unix  3      [ ]         STREAM     CONNECTED     1245073
unix  3      [ ]         STREAM     CONNECTED     1247523
unix  3      [ ]         STREAM     CONNECTED     10597
unix  3      [ ]         STREAM     CONNECTED     214701
unix  3      [ ]         STREAM     CONNECTED     1245122
unix  3      [ ]         STREAM     CONNECTED     1102187
unix  3      [ ]         STREAM     CONNECTED     10180    @/com/ubuntu/upstart
unix  3      [ ]         STREAM     CONNECTED     1102185
unix  3      [ ]         STREAM     CONNECTED     1248491  @/tmp/.X11-unix/X0
unix  2      [ ]         DGRAM                    1245153
unix  3      [ ]         STREAM     CONNECTED     1102192
unix  3      [ ]         STREAM     CONNECTED     1245080  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1248519
unix  3      [ ]         STREAM     CONNECTED     1276778
unix  3      [ ]         STREAM     CONNECTED     13590
unix  3      [ ]         STREAM     CONNECTED     1102146
unix  3      [ ]         STREAM     CONNECTED     1276877  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     13715
unix  3      [ ]         STREAM     CONNECTED     1102128
unix  3      [ ]         STREAM     CONNECTED     1102150
unix  3      [ ]         STREAM     CONNECTED     1273740
unix  3      [ ]         STREAM     CONNECTED     14931
unix  3      [ ]         STREAM     CONNECTED     1248575
unix  3      [ ]         STREAM     CONNECTED     1102193
unix  3      [ ]         STREAM     CONNECTED     1102147
unix  3      [ ]         STREAM     CONNECTED     1284620
unix  3      [ ]         STREAM     CONNECTED     1102129
unix  3      [ ]         STREAM     CONNECTED     1276779  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1273754
unix  3      [ ]         STREAM     CONNECTED     1285370  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     215882
unix  3      [ ]         STREAM     CONNECTED     1102151
unix  3      [ ]         STREAM     CONNECTED     1285342
unix  3      [ ]         STREAM     CONNECTED     10663
unix  3      [ ]         STREAM     CONNECTED     1276876
unix  3      [ ]         STREAM     CONNECTED     1102127
unix  3      [ ]         STREAM     CONNECTED     1247532
unix  3      [ ]         STREAM     CONNECTED     1102153
unix  3      [ ]         STREAM     CONNECTED     1102177
unix  3      [ ]         STREAM     CONNECTED     13008    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1102164
unix  3      [ ]         STREAM     CONNECTED     1102156
unix  3      [ ]         STREAM     CONNECTED     1247536
unix  3      [ ]         STREAM     CONNECTED     1102161
unix  3      [ ]         STREAM     CONNECTED     1102179
unix  3      [ ]         STREAM     CONNECTED     1102184
unix  3      [ ]         STREAM     CONNECTED     60822    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     61588
unix  3      [ ]         STREAM     CONNECTED     1102180
unix  3      [ ]         STREAM     CONNECTED     1102168
unix  3      [ ]         STREAM     CONNECTED     1102157
unix  3      [ ]         STREAM     CONNECTED     1285402  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1247598
unix  3      [ ]         STREAM     CONNECTED     1247599
unix  3      [ ]         STREAM     CONNECTED     1102165
unix  3      [ ]         STREAM     CONNECTED     1102160
unix  3      [ ]         STREAM     CONNECTED     1102176
unix  2      [ ]         DGRAM                    60817
unix  3      [ ]         STREAM     CONNECTED     1248579  @/tmp/.X11-unix/X0
unix  3      [ ]         STREAM     CONNECTED     61583    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     13229    /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    12905
unix  3      [ ]         STREAM     CONNECTED     61589    /var/run/dbus/system_bus_socket
unix  2      [ ]         DGRAM                    17816
unix  2      [ ]         DGRAM                    12807
unix  2      [ ]         DGRAM                    12292
unix  3      [ ]         STREAM     CONNECTED     166206   /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1247604
unix  3      [ ]         STREAM     CONNECTED     1246436
unix  2      [ ]         DGRAM                    1333813
unix  3      [ ]         STREAM     CONNECTED     1247601  @/tmp/.X11-unix/X0
unix  3      [ ]         STREAM     CONNECTED     1102140
unix  3      [ ]         STREAM     CONNECTED     166205
unix  3      [ ]         STREAM     CONNECTED     12742
unix  3      [ ]         STREAM     CONNECTED     12294
unix  3      [ ]         STREAM     CONNECTED     1333823  private/smtp
unix  3      [ ]         STREAM     CONNECTED     12799
unix  3      [ ]         STREAM     CONNECTED     1245908
unix  2      [ ]         DGRAM                    61712
unix  3      [ ]         STREAM     CONNECTED     12300
unix  3      [ ]         STREAM     CONNECTED     1102142
unix  3      [ ]         STREAM     CONNECTED     1102143
unix  3      [ ]         STREAM     CONNECTED     12793
unix  3      [ ]         STREAM     CONNECTED     12295
unix  3      [ ]         STREAM     CONNECTED     1102135
unix  3      [ ]         STREAM     CONNECTED     1247506
unix  3      [ ]         STREAM     CONNECTED     10602
unix  3      [ ]         STREAM     CONNECTED     1245074  /var/run/dbus/system_bus_socket
unix  3      [ ]         DGRAM                    214634
unix  3      [ ]         STREAM     CONNECTED     1102189
unix  3      [ ]         STREAM     CONNECTED     1102186
unix  3      [ ]         STREAM     CONNECTED     12377
unix  2      [ ]         DGRAM                    214686
unix  2      [ ]         STREAM     CONNECTED     1294682
unix  2      [ ]         DGRAM                    13227
unix  2      [ ]         DGRAM                    1245121
unix  3      [ ]         STREAM     CONNECTED     215935   /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     10648
unix  3      [ ]         STREAM     CONNECTED     1102190
unix  3      [ ]         STREAM     CONNECTED     1245123  /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1245079
unix  3      [ ]         STREAM     CONNECTED     12378    /var/run/dbus/system_bus_socket
unix  3      [ ]         STREAM     CONNECTED     1102191
```

### SHOW ARP

Sample output:

```
-- CURRENT ARP TABLE --
Address                  HWtype  HWaddress           Flags Mask            Iface
172.25.2.1               ether   18:03:73:ad:e7:57   C                     wlan0
172.25.3.51              ether   c0:4a:00:29:6f:85   C                     wlan0
172.25.3.43              ether   b8:8d:12:2f:f0:62   C                     wlan0
172.25.2.103             ether   68:a8:6d:24:4e:2a   C                     wlan0
172.25.3.110             ether   18:ee:69:25:b7:34   C                     wlan0
hagane.cs.fiu.edu        ether   b8:ca:3a:79:c2:eb   C                     eth0
172.25.2.240             ether   00:26:bb:07:5b:02   C                     wlan0
pissinou-pc.cs.fiu.edu   ether   00:25:64:b3:1c:1a   C                     eth0
172.25.2.198             ether   60:f8:1d:a8:51:de   C                     wlan0
172.25.3.55              ether   b8:09:8a:c6:a2:23   C                     wlan0
cr1.cs.fiu.edu           ether   00:1c:73:69:04:a7   C                     eth0
172.25.3.71              ether   c8:69:cd:af:0d:ca   C                     wlan0
icave-render.cs.fiu.edu  ether   a0:36:9f:78:67:c2   C                     eth0
172.25.2.224             ether   a4:5e:60:e9:3f:17   C                     wlan0
sagwa.cs.fiu.edu         ether   00:22:22:22:22:07   C                     eth0
zorba.cs.fiu.edu         ether   00:60:dd:46:b4:ea   C                     eth0
inet.cs.fiu.edu          ether   b8:ac:6f:1f:31:38   C                     eth0
172.25.2.85              ether   c8:bc:c8:e8:43:ba   C                     wlan0
172.25.2.239             ether   6c:40:08:8c:20:ca   C                     wlan0
vdr-130-0.cs.fiu.edu     ether   00:00:5e:00:01:20   C                     eth0
Connection closed by foreign host.
```

### SHOW PROC

Sample output:

```
-- RUNNING PROCESSES: --
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root         1  0.0  0.1   4476  1820 ?        Ss   Jul19   0:18 /sbin/init
root         2  0.0  0.0      0     0 ?        S    Jul19   0:00 [kthreadd]
root         3  0.0  0.0      0     0 ?        S    Jul19   9:12 [ksoftirqd/0]
root         5  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kworker/0:0H]
root         7  0.2  0.0      0     0 ?        S    Jul19  37:49 [rcu_sched]
root         8  0.0  0.0      0     0 ?        S    Jul19   0:00 [rcu_bh]
root         9  0.3  0.0      0     0 ?        S    Jul19  43:07 [migration/0]
root        10  0.0  0.0      0     0 ?        S    Jul19   0:55 [watchdog/0]
root        11  0.0  0.0      0     0 ?        S    Jul19   1:11 [watchdog/1]
root        12  0.3  0.0      0     0 ?        S    Jul19  49:26 [migration/1]
root        13  0.0  0.0      0     0 ?        S    Jul19   8:54 [ksoftirqd/1]
root        15  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kworker/1:0H]
root        16  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [khelper]
root        17  0.0  0.0      0     0 ?        S    Jul19   0:00 [kdevtmpfs]
root        18  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [netns]
root        19  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [perf]
root        20  0.0  0.0      0     0 ?        S    Jul19   0:01 [khungtaskd]
root        21  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [writeback]
root        22  0.0  0.0      0     0 ?        SN   Jul19   0:00 [ksmd]
root        23  0.0  0.0      0     0 ?        SN   Jul19   6:46 [khugepaged]
root        24  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [crypto]
root        25  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kintegrityd]
root        26  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [bioset]
root        27  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kblockd]
root        29  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [ata_sff]
root        30  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [md]
root        31  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [devfreq_wq]
root        34  0.0  0.0      0     0 ?        S    Jul19   0:14 [kswapd0]
root        35  0.0  0.0      0     0 ?        S    Jul19   0:00 [fsnotify_mark]
root        36  0.0  0.0      0     0 ?        S    Jul19   0:00 [ecryptfs-kthrea]
root        48  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kthrotld]
root        49  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [acpi_thermal_pm]
root        51  0.0  0.0      0     0 ?        S    Jul19   0:00 [scsi_eh_0]
root        52  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [scsi_tmf_0]
root        53  0.0  0.0      0     0 ?        S    Jul19   0:00 [scsi_eh_1]
root        54  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [scsi_tmf_1]
root        55  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [ipv6_addrconf]
root        76  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [deferwq]
root        77  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [charger_manager]
root       122  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kpsmoused]
root       216  0.0  0.0      0     0 ?        S\<   Jul19   0:24 [kworker/0:1H]
root       217  0.0  0.0      0     0 ?        S    Jul19   0:42 [jbd2/sda1-8]
root       218  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [ext4-rsv-conver]
message+   425  0.0  0.2   4880  2296 ?        Ss   Jul19   5:21 dbus-daemon --system --fork
root       472  0.0  0.1   4940  1656 ?        Ss   Jul19   0:00 /usr/sbin/bluetoothd
root       488  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [krfcommd]
syslog     492  0.0  0.0  32404   840 ?        Ssl  Jul19   0:29 rsyslogd
avahi      501  0.0  0.1   3816  1240 ?        S    Jul19   9:28 avahi-daemon: running [ubi-tan.local]
avahi      504  0.0  0.0   3484    24 ?        S    Jul19   0:00 avahi-daemon: chroot helper
root       505  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [cfg80211]
root       567  0.0  0.1   8868  1828 ?        Ss   Jul19   0:00 /usr/sbin/cups-browsed
root       572  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kmpathd]
root       573  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [kmpath_handlerd]
root       605  0.0  0.0   3160   432 ?        S    Jul19   0:03 upstart-file-bridge --daemon
root       622  0.0  0.0      0     0 ?        S\<   Jul19   0:00 [hd-audio0]
root       667  0.0  0.1  33272  1164 ?        Ss   Jul19   0:05 smbd -F
root       712  0.0  0.0  31460     4 ?        S    Jul19   0:00 smbd -F
root       726  0.0  0.3  33372  3832 ?        S    Jul19   0:06 smbd -F
root       739  0.0  0.1  38520  1740 ?        Ssl  Jul19   0:00 /usr/sbin/ModemManager
root       786  0.0  0.4  53920  4212 ?        Ssl  Jul19  11:55 NetworkManager
root       821  0.0  0.3  35984  4052 ?        Sl   Jul19   0:01 /usr/lib/policykit-1/polkitd --no-debug
root       880  0.0  0.0   4656    44 tty4     Ss+  Jul19   0:00 /sbin/getty -8 38400 tty4
root       882  0.0  0.0   4656    44 tty5     Ss+  Jul19   0:00 /sbin/getty -8 38400 tty5
root       888  0.0  0.0   4656    48 tty2     Ss+  Jul19   0:00 /sbin/getty -8 38400 tty2
root       889  0.0  0.0   4656    44 tty3     Ss+  Jul19   0:00 /sbin/getty -8 38400 tty3
root       893  0.0  0.0   4656    44 tty6     Ss+  Jul19   0:00 /sbin/getty -8 38400 tty6
root       940  0.0  0.1   7816  1060 ?        Ss   Jul19   0:00 /usr/sbin/sshd -D
root       949  0.0  0.0   3064   388 ?        Ss   Jul19   0:02 cron
root       962  0.0  0.0   2208   944 ?        Ss   Jul19   0:00 acpid -c /etc/acpi/events -s /var/run/acpid.socket
kernoops  1093  0.0  0.0   6400   412 ?        Ss   Jul19   0:47 /usr/sbin/kerneloops
root      1105  0.0  0.2  20952  2656 ?        Ss   Jul19   0:00 mdm
root      1184  0.0  0.0   3412   528 ?        S    Jul19   0:02 upstart-socket-bridge --daemon
root      1472  0.5  0.0      0     0 ?        S    Jul19  69:21 [irq/17-b43]
root      1477  0.0  0.1   7064  1268 ?        Ss   Jul19   3:30 /sbin/wpa_supplicant -B -P /run/sendsigs.omit.d/wpasupplicant.pid -u -s -O /var/run/wpa_supplicant
root      1593  0.0  0.1   5524  1312 ?        S    Jul19   0:00 /sbin/dhclient -d -sf /usr/lib/NetworkManager/nm-dhcp-client.action -pf /run/sendsigs.omit.d/network-manager.dhclient-eth0.pid -lf /var/lib/NetworkManager/dhclient-de130293-6e0d-45b6-8775-5c3c64b4984e-eth0.lease -cf /var/lib/NetworkManager/dhclient-eth0.conf eth0
root      1606  0.0  0.0   4656    48 tty1     Ss+  Jul19   0:00 /sbin/getty -8 38400 tty1
nobody    1644  0.0  0.0   5560   352 ?        S    Jul19   0:12 /usr/sbin/dnsmasq --no-resolv --keep-in-foreground --no-hosts --bind-interfaces --pid-file=/run/sendsigs.omit.d/network-manager.dnsmasq.pid --listen-address=127.0.1.1 --conf-file=/var/run/NetworkManager/dnsmasq.conf --cache-size=0 --proxy-dnssec --enable-dbus=org.freedesktop.NetworkManager.dnsmasq --conf-dir=/etc/NetworkManager/dnsmasq.d
root      1786  0.0  0.1  20300  1556 ?        Ss   Jul19  13:02 nmbd -D
root      1791  0.0  0.0      0     0 ?        S    Jul19   0:00 [kauditd]
root      1810  0.0  0.1   5524  1420 ?        S    Jul19   0:00 /sbin/dhclient -d -sf /usr/lib/NetworkManager/nm-dhcp-client.action -pf /run/sendsigs.omit.d/network-manager.dhclient-wlan0.pid -lf /var/lib/NetworkManager/dhclient-c3c37114-d954-41bc-a018-cfd9b31450e6-wlan0.lease -cf /var/lib/NetworkManager/dhclient-wlan0.conf wlan0
root      2148  0.0  0.0      0     0 ?        S    22:59   0:01 [kworker/u4:3]
root      2234  0.0  0.0      0     0 ?        S    23:10   0:01 [kworker/u4:2]
root      2433  0.0  0.0      0     0 ?        S    23:34   0:00 [kworker/u4:1]
postfix   2434  0.0  0.2   4816  2632 ?        S    23:34   0:00 pickup -l -t unix -u -c
root      2474  0.0  0.0      0     0 ?        S    23:40   0:00 [kworker/u4:0]
ltorr121  2484  0.2  0.7  21184  7884 pts/3    Sl+  23:42   0:00 python tcp_server.py
root      2488  0.0  0.2   3652  2428 pts/6    S+   23:43   0:00 telnet localhost 9999
ltorr121  2490  0.0  0.2   5232  2324 pts/3    R+   23:43   0:00 ps -aux
root      5047  0.0  0.3  31076  4008 ?        Sl   Jul20   0:03 /usr/sbin/console-kit-daemon --no-daemon
colord    5145  0.0  0.2  37344  2736 ?        Sl   Jul20   0:00 /usr/lib/colord/colord
root      7307  0.0  0.0   3024   972 ?        S    Jul23   0:01 upstart-udev-bridge --daemon
root      7310  0.0  0.1  13008  1156 ?        Ss   Jul23   0:00 /lib/systemd/systemd-udevd --daemon
root      7379  0.0  0.2   4224  2268 ?        Ss   Jul23   0:00 /lib/systemd/systemd-logind
mysql     7633  0.2  1.0 340048 10484 ?        Ssl  Jul23  18:38 /usr/sbin/mysqld
root      7728  0.0  0.0 144048   308 ?        Ss   Jul25   0:25 /usr/sbin/apache2 -k start
www-data  7732  0.0  0.1 154500  1636 ?        S    Jul25   1:06 /usr/sbin/apache2 -k start
www-data  7733  0.0  0.1 156684  1256 ?        S    Jul25   1:11 /usr/sbin/apache2 -k start
www-data  7736  0.0  0.0 158132   932 ?        S    Jul25   0:16 /usr/sbin/apache2 -k start
root      8718  0.0  0.0      0     0 ?        S\<   Jul23   0:00 [xfsalloc]
root      8719  0.0  0.0      0     0 ?        S\<   Jul23   0:00 [xfs_mru_cache]
root      8723  0.0  0.0      0     0 ?        S    Jul23   0:00 [jfsIO]
root      8724  0.0  0.0      0     0 ?        S    Jul23   0:00 [jfsCommit]
root      8725  0.0  0.0      0     0 ?        S    Jul23   0:00 [jfsCommit]
root      8726  0.0  0.0      0     0 ?        S    Jul23   0:00 [jfsSync]
root      8745  0.0  0.0      0     0 ?        S\<   Jul23   0:00 [bioset]
www-data 10352  0.0  0.0 158536   728 ?        S    Jul25   0:57 /usr/sbin/apache2 -k start
www-data 10443  0.0  0.1 157524  1936 ?        S    Jul25   0:06 /usr/sbin/apache2 -k start
www-data 10445  0.0  0.1 155136  1600 ?        S    Jul25   0:07 /usr/sbin/apache2 -k start
root     11937  0.0  0.2   8312  2636 ?        Ss   Jul23   0:01 /usr/sbin/cupsd -f
root     13667  0.0  0.0      0     0 ?        S\<   Jul23   0:00 [kworker/1:1H]
ltorr121 19305  0.0  0.0   5720   560 ?        Ss   Jul26   0:00 SCREEN
ltorr121 19306  0.0  0.0   7004   608 pts/5    Ss   Jul26   0:00 /bin/bash
ltorr121 19322  0.0  1.5 131904 16168 pts/5    Sl+  Jul26   0:05 node app
root     20397  0.0  0.0   5720     0 ?        Ss   Jul23   0:00 SCREEN
root     20398  0.0  0.0   6844    48 pts/2    Ss   Jul23   0:00 /bin/bash
root     20414  1.0  3.8 320752 39336 pts/2    Sl+  Jul23  77:31 mongod
www-data 22570  0.0  0.1 158068  1824 ?        S    Jul27   0:10 /usr/sbin/apache2 -k start
root     23940  0.0  0.0   4796   900 ?        Ss   Jul27   0:01 /usr/lib/postfix/master
postfix  24167  0.0  0.1   4956  1316 ?        S    Jul27   0:00 qmgr -l -t unix -u
www-data 24287  0.0  0.0 160160   840 ?        S    Jul27   0:08 /usr/sbin/apache2 -k start
www-data 24288  0.0  0.1 144104  1448 ?        S    Jul27   0:00 /usr/sbin/apache2 -k start
www-data 24292  0.0  0.1 144112  1148 ?        S    Jul27   0:00 /usr/sbin/apache2 -k start
postfix  25270  0.0  0.1   7108  1636 ?        S    Jul27   0:00 tlsmgr -l -t unix -u -c
root     30365  0.0  0.5  37820  5816 ?        Sl   11:19   0:00 /usr/lib/upower/upowerd
rtkit    30380  0.0  0.2  21372  2200 ?        SNl  11:19   0:01 /usr/lib/rtkit/rtkit-daemon
root     30576  0.0  0.5  52412  5852 ?        Sl   11:19   0:19 /usr/lib/udisks2/udisksd --no-debug
root     30640  0.0  0.7  36420  7644 ?        Sl   11:19   0:01 /usr/lib/accountsservice/accounts-daemon
root     30831  0.0  0.5  23796  5524 ?        S    11:24   0:00 mdm
root     30836  5.1 29.8 1339164 303456 tty7   Ss+  11:24  38:15 /usr/bin/X :0 -audit 0 -auth /var/lib/mdm/:0.Xauth -nolisten tcp vt7
root     30845  0.0  0.1   3784  1484 ?        Ss   11:24   0:00 /usr/bin/syndaemon -d -i 1.0 -t -K -R
mdm      30846 61.9 23.9 1751280 243256 ?      Ssl  11:24 458:02 /usr/lib/mdm/mdmwebkit
mdm      30860  0.0  0.1   3860  1456 ?        S    11:24   0:00 dbus-launch --autolaunch=c47e46bd315a7008c3a47f3956e1d891 --binary-syntax --close-stderr
mdm      30868  0.0  0.0   4248    56 ?        Ss   11:24   0:00 //bin/dbus-daemon --fork --print-pid 5 --print-address 7 --session
root     31113  0.1  0.0      0     0 ?        S    12:01   0:48 [kworker/0:0]
root     31785  0.0  0.0      0     0 ?        S    14:07   0:18 [kworker/1:2]
root     31786  0.6  0.0      0     0 ?        S    14:07   3:34 [kworker/0:1]
root     32102  0.0  0.6  13320  6492 ?        Ss   15:29   0:00 sshd: ltorr121 [priv]
root     32109  0.3  0.0      0     0 ?        S    15:29   1:49 [kworker/1:1]
ltorr121 32119  0.0  0.4  13320  4652 ?        S    15:29   0:10 sshd: ltorr121@pts/3
ltorr121 32120  0.0  0.4   7188  4524 pts/3    Ss   15:29   0:02 -bash
root     32404  0.0  0.5  13324  5984 ?        Ss   16:36   0:00 sshd: ltorr121 [priv]
ltorr121 32415  0.0  0.4  13324  4184 ?        S    16:37   0:05 sshd: ltorr121@pts/6
ltorr121 32416  0.0  0.4   7068  4852 pts/6    Ss   16:37   0:00 -bash
root     32434  0.0  0.4   8420  4076 pts/6    S    16:37   0:00 su
root     32441  0.0  0.4   7140  4976 pts/6    S    16:37   0:01 bash
```

### SHOW FS

Sample output:

```
-- FILE SYSTEM: --
total 459368
drwxr-xr-x  31 ltorr121 ltorr121      4096 Jul 28 23:06 .
drwxr-xr-x   3 root     root          4096 Mar 10 15:14 ..
drwx------   3 ltorr121 ltorr121      4096 Mar 31 17:27 .adobe
-rw-rw-r--   1 ltorr121 ltorr121      4814 Jul 28 16:53 back_up_of_tcpserv.py
-rw-------   1 ltorr121 ltorr121     20288 Jul 28 14:07 .bash_history
-rw-r--r--   1 ltorr121 ltorr121       220 Mar 10 15:14 .bash_logout
-rw-r--r--   1 ltorr121 ltorr121 301542593 Jul 13 14:12 Books.tar.gz
drwx------  10 ltorr121 ltorr121      4096 Jul 28 11:19 .cache
-rw-rw-r--   1 ltorr121 ltorr121    278688 Jul 25 17:07 cjcl1990-parker-e875b79c5280.zip
-rw-rw-r--   1 ltorr121 ltorr121  19415832 Jul 25 17:07 cjcl1990-parkerweb-581852cb5900.zip
drwxr-xr-x  14 ltorr121 ltorr121      4096 Jun 24 13:30 .config
-rw-rw-r--   1 ltorr121 ltorr121       243 Jul 23 22:11 .dbshell
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Desktop
-rw-------   1 ltorr121 ltorr121        25 Jul 28 11:19 .dmrc
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Documents
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Downloads
-rw-rw-r--   1 ltorr121 ltorr121       996 Jul 27 15:26 example_from_internet.py
-rw-rw-r--   1 ltorr121 ltorr121      1485 Jul 27 16:59 extra_credit_sqlizers.zip
drwx------   3 ltorr121 ltorr121      4096 Jul 14 15:22 .freerdp
drwx------   3 ltorr121 ltorr121      4096 Jul 28 11:19 .gconf
-rw-r-----   1 ltorr121 ltorr121         0 Mar 10 15:30 .gksu.lock
drwx------   3 ltorr121 ltorr121      4096 Mar 10 15:43 .gnome2
drwx------   2 ltorr121 ltorr121      4096 Mar 10 15:43 .gnome2_private
drwxr-xr-x   2 ltorr121 ltorr121      4096 Jun 28 12:49 .gstreamer-0.10
-rw-r--r--   1 ltorr121 ltorr121        22 Mar 10 15:14 .gtkrc-2.0
-rw-r--r--   1 ltorr121 ltorr121       516 Mar 10 15:14 .gtkrc-xfce
-rw-------   1 ltorr121 ltorr121       644 Jul 28 11:24 .ICEauthority
drwx------   2 ltorr121 ltorr121      4096 Jul 23 14:53 .irssi
drwxr-xr-x   4 ltorr121 ltorr121      4096 Mar 10 15:28 .linuxmint
drwxr-xr-x   3 ltorr121 ltorr121      4096 Mar 10 15:14 .local
drwx------   3 ltorr121 ltorr121      4096 Jun 24 13:12 .macromedia
-rw-rw-r--   1 ltorr121 ltorr121    462580 Jul 26 21:28 master.zip
-rw-r--r--   1 root     root      71913688 Jul 12 14:50 mongodb-linux-i686-3.2.8.tgz
-rw-rw-r--   1 ltorr121 ltorr121  75154873 Jul 12 14:47 mongodb-linux-x86_64-3.2.8.tgz
-rw-rw-r--   1 ltorr121 ltorr121         0 Jul 23 16:52 .mongorc.js
drwxr-xr-x   4 ltorr121 ltorr121      4096 Mar 10 15:43 .mozilla
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Music
-rw-------   1 ltorr121 ltorr121        22 Jul  8 12:31 .mysql_history
-rw-------   1 ltorr121 ltorr121        40 Jul 28 23:42 .nano_history
drwxrwxr-x   3 ltorr121 ltorr121      4096 Jul 26 21:30 .node-gyp
drwxrwxr-x   8 ltorr121 ltorr121      4096 Jul 26 21:29 Node.js_UserLogin_Template-master
drwxrwxr-x 172 ltorr121 ltorr121      4096 Jul 26 21:29 .npm
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:54 Pictures
drwxr-xr-x   2 root     root          4096 Jul 23 17:30 preferences
-rw-r--r--   1 ltorr121 ltorr121       675 Mar 10 15:14 .profile
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Public
drwx------   5 ltorr121 ltorr121      4096 Jun 28 14:10 .purple
-rw-rw-r--   1 ltorr121 ltorr121       443 Jul 27 16:56 README.txt
drwx------   2 ltorr121 ltorr121      4096 Jul 14 15:30 .remmina
-rw-r--r--   1 ltorr121 ltorr121       638 Jul 18 13:32 Resources_For_SQL_Video.txt
drwx------   2 ltorr121 ltorr121      4096 Jun 24 12:45 .ssh
-rwxrwxr-x   1 ltorr121 ltorr121        47 Jul 28 23:05 stupid.sh
-rw-rw-r--   1 ltorr121 ltorr121      5343 Jul 28 23:42 tcp_server.py
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Templates
-rw-rw-r--   1 ltorr121 ltorr121        12 Jul 28 23:06 test.py
-rw-rw-r--   1 ltorr121 ltorr121        10 Jul 28 18:16 test.txt
drwxr-xr-x   3 ltorr121 ltorr121      4096 Mar 10 15:32 .thumbnails
-rw-r--r--   1 ltorr121 ltorr121   1311427 Jul  8 12:25 v1.9.zip
drwxr-xr-x   2 ltorr121 ltorr121      4096 Mar 10 15:27 Videos
-rw-r--r--   1 ltorr121 ltorr121      3951 Jul 18 13:32 Video_Script_REV1
-rw-r--r--   1 ltorr121 ltorr121     16502 Jul 28 11:24 .xfce4-session.verbose-log
-rw-r--r--   1 ltorr121 ltorr121     16502 Jul 14 16:16 .xfce4-session.verbose-log.last
-rw-r--r--   1 ltorr121 ltorr121      7572 Mar 10 16:14 .xscreensaver
-rw-r--r--   1 ltorr121 ltorr121       251 Jul 28 11:24 .xsession-errors

CURRENT DIRECTORY: /home/ltorr121
```

### SHOW DEV

Sample output:

```
-- PCI DEVICES: --
00:00.0 Host bridge: Intel Corporation Mobile 945GSE Express Memory Controller Hub (rev 03)
00:02.0 VGA compatible controller: Intel Corporation Mobile 945GSE Express Integrated Graphics Controller (rev 03)
00:02.1 Display controller: Intel Corporation Mobile 945GM/GMS/GME, 943/940GML Express Integrated Graphics Controller (rev 03)
00:1b.0 Audio device: Intel Corporation NM10/ICH7 Family High Definition Audio Controller (rev 02)
00:1c.0 PCI bridge: Intel Corporation NM10/ICH7 Family PCI Express Port 1 (rev 02)
00:1c.1 PCI bridge: Intel Corporation NM10/ICH7 Family PCI Express Port 2 (rev 02)
00:1c.2 PCI bridge: Intel Corporation NM10/ICH7 Family PCI Express Port 3 (rev 02)
00:1d.0 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #1 (rev 02)
00:1d.1 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #2 (rev 02)
00:1d.2 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #3 (rev 02)
00:1d.3 USB controller: Intel Corporation NM10/ICH7 Family USB UHCI Controller #4 (rev 02)
00:1d.7 USB controller: Intel Corporation NM10/ICH7 Family USB2 EHCI Controller (rev 02)
00:1e.0 PCI bridge: Intel Corporation 82801 Mobile PCI Bridge (rev e2)
00:1f.0 ISA bridge: Intel Corporation 82801GBM (ICH7-M) LPC Interface Bridge (rev 02)
00:1f.2 IDE interface: Intel Corporation 82801GBM/GHM (ICH7-M Family) SATA Controller [IDE mode] (rev 02)
00:1f.3 SMBus: Intel Corporation NM10/ICH7 Family SMBus Controller (rev 02)
03:00.0 Network controller: Broadcom Corporation BCM4312 802.11b/g LP-PHY (rev 01)
04:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8101/2/6E PCI Express Fast/Gigabit Ethernet controller (rev 02)
```


## Other

### EXEC

Example: `EXEC:nmap -v localhost`

Sample output:

```
Starting Nmap 6.40 ( http://nmap.org ) at 2016-07-28 23:50 EDT
Initiating Ping Scan at 23:50
Scanning localhost (127.0.0.1) [2 ports]
Completed Ping Scan at 23:50, 0.00s elapsed (1 total hosts)
Initiating Connect Scan at 23:50
Scanning localhost (127.0.0.1) [1000 ports]
Discovered open port 3306/tcp on 127.0.0.1
Discovered open port 25/tcp on 127.0.0.1
Discovered open port 80/tcp on 127.0.0.1
Discovered open port 445/tcp on 127.0.0.1
Discovered open port 8080/tcp on 127.0.0.1
Discovered open port 139/tcp on 127.0.0.1
Discovered open port 22/tcp on 127.0.0.1
Discovered open port 631/tcp on 127.0.0.1
Discovered open port 9999/tcp on 127.0.0.1
Completed Connect Scan at 23:50, 0.14s elapsed (1000 total ports)
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0021s latency).
Not shown: 991 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
25/tcp   open  smtp
80/tcp   open  http
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
631/tcp  open  ipp
3306/tcp open  mysql
8080/tcp open  http-proxy
9999/tcp open  abyss

Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.33 seconds
```

### SNIFF

Example: `SNIFF`

Sample output:

```text
[

  dst=00:00:5e:00:01:20 src=00:24:e8:cf:31:aa type=IPv4 |\<IP  version=4L ihl=5L tos=0x10 len=180 id=38528 flags=DF frag=0L ttl=64 proto=tcp chksum=0xb310 src=131.94.130.30 dst=73.139.161.155 options=[] |\<TCP  sport=ssh dport=40302 seq=1757865738 ack=4238477273L dataofs=8L reserved=0L flags=PA window=316 chksum=0x675a urgptr=0 options=[('NOP', None), ('NOP', None), ('Timestamp', (261732324, 988139))] |\<Raw  load='\xc2\x04s\x06 \x17\xec\x95\x0fv\x85\xe9\xebY\xdb%\xd4\xd0\x84\xb1l\x0c\x0f\xbes\xcd\xad\x93\xa8\x88\x8b>\xcb\xb2\x11OD\xc7\xd88tB\xdeVB9Y\xb6\xa7\x05\xa7j$\xa9Z\x1aI\xc2+8\x91T\x14s\xdb\xacj\x98\x0c\xc0~_\xd0K=\xa3\xe3Y\xefP=L\x08\xaf~Nm\xf3\x94h ;\xef"&\xe4S>\x12\x08\x8b\x7f\xe1\xba\xdc\xb5=jq\xe8\x87A\xf5\xc7\xb7=Gd\x99f\xf6W;\xf7\xb8\xf1\xa9a' |>>>>, 

  dst=33:33:ff:30:00:51 src=18:66:da:0a:1a:33 type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=32 nh=ICMPv6 hlim=255 src=fe80::1a66:daff:fe0a:1a33 dst=ff02::1:ff30:51 |\<ICMPv6ND_NS  type=Neighbor Solicitation code=0 cksum=0x9752 R=0L S=0L O=0L res=0x0L tgt=2001:468:701:4002:5c15:0:130:51 |\<ICMPv6NDOptSrcLLAddr  type=1 len=1 lladdr=18:66:da:0a:1a:33 |>>>>, 

  dst=33:33:ff:30:00:51 src=78:2b:cb:ae:e7:fa type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=32 nh=ICMPv6 hlim=255 src=fe80::5b7:7434:1a49:eb12 dst=ff02::1:ff30:51 |\<ICMPv6ND_NS  type=Neighbor Solicitation code=0 cksum=0x67e R=0L S=0L O=0L res=0x0L tgt=2001:468:701:4002:5c15:0:130:51 |\<ICMPv6NDOptSrcLLAddr  type=1 len=1 lladdr=78:2b:cb:ae:e7:fa |>>>>, 

  dst=01:00:5e:00:00:12 src=00:00:5e:00:01:20 type=IPv4 |\<IP  version=4L ihl=5L tos=0xc0 len=40 id=1 flags= frag=0L ttl=255 proto=vrrp chksum=0xd531 src=131.94.130.2 dst=224.0.0.18 options=[] |\<VRRP  version=2L type=1L vrid=32 priority=110 ipcount=1 authtype=0 adv=1 chksum=0x6b7a addrlist=['131.94.130.4'] auth1=0 auth2=0 |\<Padding  load='\x00\x00\x00\x00\x00\x00' |>>>>, 

  dst=ff:ff:ff:ff:ff:ff src=00:1c:73:69:04:a7 type=ARP |\<ARP  hwtype=0x1 ptype=IPv4 hwlen=6 plen=4 op=who-has hwsrc=00:1c:73:69:04:a7 psrc=131.94.130.2 hwdst=00:00:00:00:00:00 pdst=131.94.130.140 |\<Padding  load='\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00' |>>>, 

  dst=33:33:00:01:00:02 src=18:66:da:0a:13:0f type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=110 nh=UDP hlim=1 src=fe80::1a66:daff:fe0a:130f dst=ff02::1:2 |\<UDP  sport=dhcpv6_client dport=dhcpv6_server len=110 chksum=0x469e |\<DHCP6_Solicit  msgtype=SOLICIT trid=0x50a737 |\<DHCP6OptElapsedTime  optcode=ELAPSED_TIME optlen=2 elapsedtime=1.00 sec |\<DHCP6OptClientId  optcode=CLIENTID optlen=14 duid=\<DUID_LLT  type=Link-layer address plus time hwtype=Ethernet (10Mb) timeval=Thu, 02 Jun 2016 18:09:02 +0000 (1464890942) lladdr=18:66:da:0a:13:0f |> |\<DHCP6OptIA_NA  optcode=IA_NA optlen=12 iaid=0x1c1866da T1=0 T2=0 ianaopts=[] |\<DHCP6OptClientFQDN  optcode=OPTION_CLIENT_FQDN optlen=24 res=0L flags= fqdn='joffrey.ad.cs.fiu.edu' |\<DHCP6OptVendorClass  optcode=VENDOR_CLASS optlen=14 enterprisenum=Microsoft vcdata=[\<VENDOR_CLASS_DATA  len=8 data='MSFT 5.0' |>] |\<DHCP6OptOptReq  optcode=ORO optlen=8 reqopts=[Domain Search List option, DNS Recursive Name Server Option, VENDOR_OPTS, OPTION_CLIENT_FQDN] |>>>>>>>>>>, 

  dst=33:33:00:01:00:02 src=18:66:da:0a:13:0f type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=110 nh=UDP hlim=1 src=fe80::1a66:daff:fe0a:130f dst=ff02::1:2 |\<UDP  sport=dhcpv6_client dport=dhcpv6_server len=110 chksum=0x469e |\<DHCP6_Solicit  msgtype=SOLICIT trid=0x50a737 |\<DHCP6OptElapsedTime  optcode=ELAPSED_TIME optlen=2 elapsedtime=1.00 sec |\<DHCP6OptClientId  optcode=CLIENTID optlen=14 duid=\<DUID_LLT  type=Link-layer address plus time hwtype=Ethernet (10Mb) timeval=Thu, 02 Jun 2016 18:09:02 +0000 (1464890942) lladdr=18:66:da:0a:13:0f |> |\<DHCP6OptIA_NA  optcode=IA_NA optlen=12 iaid=0x1c1866da T1=0 T2=0 ianaopts=[] |\<DHCP6OptClientFQDN  optcode=OPTION_CLIENT_FQDN optlen=24 res=0L flags= fqdn='joffrey.ad.cs.fiu.edu' |\<DHCP6OptVendorClass  optcode=VENDOR_CLASS optlen=14 enterprisenum=Microsoft vcdata=[\<VENDOR_CLASS_DATA  len=8 data='MSFT 5.0' |>] |\<DHCP6OptOptReq  optcode=ORO optlen=8 reqopts=[Domain Search List option, DNS Recursive Name Server Option, VENDOR_OPTS, OPTION_CLIENT_FQDN] |>>>>>>>>>>, 

  dst=33:33:00:01:00:02 src=18:66:da:0a:13:0f type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=110 nh=UDP hlim=1 src=fe80::1a66:daff:fe0a:130f dst=ff02::1:2 |\<UDP  sport=dhcpv6_client dport=dhcpv6_server len=110 chksum=0x469e |\<DHCP6_Solicit  msgtype=SOLICIT trid=0x50a737 |\<DHCP6OptElapsedTime  optcode=ELAPSED_TIME optlen=2 elapsedtime=1.00 sec |\<DHCP6OptClientId  optcode=CLIENTID optlen=14 duid=\<DUID_LLT  type=Link-layer address plus time hwtype=Ethernet (10Mb) timeval=Thu, 02 Jun 2016 18:09:02 +0000 (1464890942) lladdr=18:66:da:0a:13:0f |> |\<DHCP6OptIA_NA  optcode=IA_NA optlen=12 iaid=0x1c1866da T1=0 T2=0 ianaopts=[] |\<DHCP6OptClientFQDN  optcode=OPTION_CLIENT_FQDN optlen=24 res=0L flags= fqdn='joffrey.ad.cs.fiu.edu' |\<DHCP6OptVendorClass  optcode=VENDOR_CLASS optlen=14 enterprisenum=Microsoft vcdata=[\<VENDOR_CLASS_DATA  len=8 data='MSFT 5.0' |>] |\<DHCP6OptOptReq  optcode=ORO optlen=8 reqopts=[Domain Search List option, DNS Recursive Name Server Option, VENDOR_OPTS, OPTION_CLIENT_FQDN] |>>>>>>>>>>, 

  dst=33:33:00:01:00:02 src=18:66:da:0a:13:0f type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=110 nh=UDP hlim=1 src=fe80::1a66:daff:fe0a:130f dst=ff02::1:2 |\<UDP  sport=dhcpv6_client dport=dhcpv6_server len=110 chksum=0x469e |\<DHCP6_Solicit  msgtype=SOLICIT trid=0x50a737 |\<DHCP6OptElapsedTime  optcode=ELAPSED_TIME optlen=2 elapsedtime=1.00 sec |\<DHCP6OptClientId  optcode=CLIENTID optlen=14 duid=\<DUID_LLT  type=Link-layer address plus time hwtype=Ethernet (10Mb) timeval=Thu, 02 Jun 2016 18:09:02 +0000 (1464890942) lladdr=18:66:da:0a:13:0f |> |\<DHCP6OptIA_NA  optcode=IA_NA optlen=12 iaid=0x1c1866da T1=0 T2=0 ianaopts=[] |\<DHCP6OptClientFQDN  optcode=OPTION_CLIENT_FQDN optlen=24 res=0L flags= fqdn='joffrey.ad.cs.fiu.edu' |\<DHCP6OptVendorClass  optcode=VENDOR_CLASS optlen=14 enterprisenum=Microsoft vcdata=[\<VENDOR_CLASS_DATA  len=8 data='MSFT 5.0' |>] |\<DHCP6OptOptReq  optcode=ORO optlen=8 reqopts=[Domain Search List option, DNS Recursive Name Server Option, VENDOR_OPTS, OPTION_CLIENT_FQDN] |>>>>>>>>>>, 

  dst=33:33:00:01:00:02 src=18:66:da:0a:13:0f type=IPv6 |\<IPv6  version=6L tc=0L fl=0L plen=110 nh=UDP hlim=1 src=fe80::1a66:daff:fe0a:130f dst=ff02::1:2 |\<UDP  sport=dhcpv6_client dport=dhcpv6_server len=110 chksum=0x469e |\<DHCP6_Solicit  msgtype=SOLICIT trid=0x50a737 |\<DHCP6OptElapsedTime  optcode=ELAPSED_TIME optlen=2 elapsedtime=1.00 sec |\<DHCP6OptClientId  optcode=CLIENTID optlen=14 duid=\<DUID_LLT  type=Link-layer address plus time hwtype=Ethernet (10Mb) timeval=Thu, 02 Jun 2016 18:09:02 +0000 (1464890942) lladdr=18:66:da:0a:13:0f |> |\<DHCP6OptIA_NA  optcode=IA_NA optlen=12 iaid=0x1c1866da T1=0 T2=0 ianaopts=[] |\<DHCP6OptClientFQDN  optcode=OPTION_CLIENT_FQDN optlen=24 res=0L flags= fqdn='joffrey.ad.cs.fiu.edu' |\<DHCP6OptVendorClass  optcode=VENDOR_CLASS optlen=14 enterprisenum=Microsoft vcdata=[\<VENDOR_CLASS_DATA  len=8 data='MSFT 5.0' |>] |\<DHCP6OptOptReq  optcode=ORO optlen=8 reqopts=[Domain Search List option, DNS Recursive Name Server Option, VENDOR_OPTS, OPTION_CLIENT_FQDN] |>>>>>>>>>>]
```