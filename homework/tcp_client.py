#!/usr/bin/env python

# Some of this code is lifted from BHP.
# Thanks to walchko for uploading it to Github.

from __future__ import (print_function)
import socket
import sys
import scapy.all as sc
from six.moves import input

target_host = "ubi-tan.cs.fiu.edu"
target_port = 9999
buff = 8192


def recv_msg(client):
    total_data = []
    while True:
        data = client.recv(buff)
        if data == b'':
            break
        total_data.append(data)
    response = b''.join(total_data)

    return response.decode()


def ack_scan():
    response = ""
    ans, unans = sc.sr(sc.IP(dst=target_host) /
                       sc.TCP(dport=[80, 666], flags="A"))
    for s, r in ans:
        if s[sc.TCP].dport == r[sc.TCP].sport:
            response += str(s[sc.TCP].dport) + " is unfiltered\n"
    for s in unans:
        response += str(s[sc.TCP].dport) + " is filtered\n"

    return response


def execute(command):
    response = ""
    client = None

    try:
        # create a socket object
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # connect the client
        client.connect((target_host, target_port))

        if command == "SERVER":
            response += target_host + ":" + str(target_port)
        elif command == "ACK SCAN":
            response += ack_scan()
        else:
            client.send(command.encode())
            response += recv_msg(client)

    except socket.error as msg:
        print(str(msg))
        sys.exit(1)

    finally:
        client.close()
        return response


def main():
    try:
        while True:
            command = input(">>> ")
            if command == "CLOSE":
                break
            response = execute(command)
            print(response)

    except(KeyboardInterrupt, SystemExit, EOFError):
        print("\nInterrupt")
        sys.exit(0)

    finally:
        print("Ending session...")
        sys.exit(0)

if __name__ == "__main__":
    main()


