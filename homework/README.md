# Programming Assignment

This is assignment is an emulated environment of an infected computer using a client/server architecture (for the communication).

It is written in Python, and is compatible with both Python 2 and Python 3.
The files have been tested in Linux and are untested on other platforms.
Note also that `tcp_server.py` and `tcp_client.py` must both be run as superuser for Scapy functionality to work.

## Server

Instructions for TCP Server usage:

* This software was tested using Linux Mint 17.3 LTS, which is equivalent to Ubuntu 14.04.3 LTS, it does *NOT* work on Mac or Windows.
	- Because this was only tested on this particular distribution, you should use the Linux Mint 17.3 for optimal results.
	- It may work on other distributions, but this cannot be guaranteed.

* This server is compatible with python2.7 and python3, so as pre-requisites, make sure that that is installed on your "server machine".
	- If you do not have python installed, you can issue sudo apt-get install python OR sudo apt-get install python3 for python2.7 or python3 respectively
	- You may need extra libraries to use scapy with this software, so issue `sudo apt-get install python-scapy tcpreplay wireshark imagemagick python-gnuplot ebtables` to install them on your Ubuntu/Linux Mint installation.
		* Installing scapy for python does not install it for python3, you will need to follow the instructions below if you want to use scapy for python3
			* Scapy for Python3:
			```
				> cd /tmp
				  git clone https://github.com/phaethon/scapy
				  cd scapy
				  sudo python3 setup.py install
			```
				  
		* IMPORTANT NOTE: There is an extra library that may be required for scapy called `python-pyx`, but due to a known bug it may make the server unusable.
		
		**DO NOT INSTALL THIS LIBRARY OR IT WILL MAKE THE SERVER UNABLE TO RUN!!** 
		
		Unless there is some reason the library NEEDS to be installed. 
		If you encounter other problems, you should try installing it to see if it resolve the issue, but at the time of testing we have found no need for this library and scapy seems to run fine without it.

* If you wish to use nmap to test other commands, you can issue `sudo apt-get install nmap` to install it.
* To run the software:
	- In all cases, it requires root access to run due to scapy:
		* Python 2.7: `sudo python  tcp_server.py` 
		* Python 3: `sudo python3  tcp_server.py` 
		
	- To run as an executable:
		* `chmod +x  tcp_server.py`
		* `sudo ./tcp_server.py` 
		
* You can test whether or not the server is successfully running by telnetting to it.
	- You can use `telnet <address> <post>`
		* You should see something like `[*] Accepted connection from: 127.0.0.1:52079` on the server side
		* Typing `HLP` should yield the help menu.
			  
		* Refer to the Protocol subsection for message syntax/usage from telnet.
		
		* You must use the telnet command from a terminal, PuTTy and/or another graphical telnet client will immediately close out even though the server will acknowledge it received the connection.
		
		* Accessing from a medium other than telnet will always result in a `MESSAGE ACKNOWLEDGED` response

* Once you've tested and verified the server is running, you can use the `tcp_client.py` program to send messages back and forth to the server.

* Read the Client section to get further instructions on how to work with the server.

### Protocol

Menu:

```
HLP 		- Display this message
UP:<filename>:<data> - Upload a file
DOWN:<filename> - Download a file
SHOW IFACE 	- Show network interfaces of the server machine
SHOW NET 	- Show the host configuration
SHOW ARP 	- Show the server machine's ARP cache
SHOW PROC 	- Show running processes on server machine
SHOW FS 	- Show the structure of the filesystem on server machine
SHOW DEV 	- Show PCI Devices on the server machine
EXEC:<command> - Execute any command other than the ones given.
SNIFF 		- Show the traffic coming across the network
```

See `EXAMPLES.md` for examples of how to use the server's protocol.

## Client

The client has been tested with Python 2.7 and 3.5. To run it as an executable do:

```
chmod +x tcp_client.py
sudo ./tcp_client.py to run as executable
```

Client-specific commands:

* `CLOSE`
Ends the current session.

* `ACK SCAN`
Performs an ACK scan on the connected server.

* `SERVER`
Displays the connected server.
