\documentclass[main.tex]{subfiles}

\begin{document}
	
	\section{Background}
	
	\subsection{SQLI}
	
	We say that  SQL injection (SQLI) occurs when unprivileged users are able to
	execute their own code on a database. 
	They do this by finding some vulnerable vector of attack, then exploiting it
	by inputting whatever it is they want to run on the database. 
	The attack vectors that allow SQLI are created when the database takes 
	input from an untrusted source (e.g. a user), 
	fails to sufficiently validate and 
	sanitize the input (allowing injection), then concatenates the input to
	a premade SQL statement~\cite{halfond2006classification,chaddougherty2012}.
	A formal, rigourous definition of SQLI can be found in~\cite{su2006essence}.
	
	There are multiple ways to classify SQLI (e.g.~\cite{7012815}), but for this paper we adopt the 
	classification done by Halfond et al.~\cite{halfond2006classification}:
	
	\begin{itemize}
		\item \textbf{\textsc{Tautologies}} --
		Injected code is designed so it always evaluates to true.
		This can allow the attacker to bypass authentication, do read operations,
		and test for exploits.
		\item \textbf{\textsc{Illegal/Incorrect Queries}} --
		The injected code is designed to raise an error within the database. 
		This can allow the attacker to fingerprint the database, read data, and
		detect vulnerabilities.
		\item \textbf{\textsc{Union Query}} --
		The injected code is designed using SQL's \texttt{UNION} operator to 
		merge two or more tables.
		This can allow the attacker to bypass authentication and read data.
		\item \textbf{\textsc{Piggy-Backed Queries}} --
		The injected code is designed to so that first 
		some subset of regular code is executed, 
		then the attacker's own query is executed after. 
		This can allow the attacker to perform CRUD operations, DoS attacks, and
		execute commands.
		Also known as \textit{stacked queries}.
		\item \textbf{\textsc{Stored Procedures}} --
		The attacker injects code that exploits a vulnerability in the 
		database's stored procedures. 
		Generally this goes in tandem with another SQLI type.
		This allows the attacker to gain unauthorized privileges, DoS attacks, and
		execute commands.
		\item \textbf{\textsc{Inference}} --
		The attacker injects code that allows it to ask questions to the database.
		These questions are generally of true/false form and allow the attacker
		to receive feedback despite the lack of return or 
		error dumps from the database. 
		This allows the attacker detect vulnerabilities, performs reads, and do
		some database fingerprinting.
		\item \textbf{\textsc{Alternate Encodings}} --
		The attacker injects their own code in a different kind of encoding other
		than whatever is the standard form. 
		E.g. Unicode, ASCII, and hexadecimal.
		This allows the attacker to inject code into a database that has some (not
		sufficient) mechanism to sanitize and validate input.
	\end{itemize}
	
	The core impacts that SQLI has on web applications are the following
	\cite{6200667}:
	
	\begin{itemize}
		\item Confidentiality -- The compromise of confidential data.
		\item Integrity -- The compromise of database integrity.
		\item Authentication -- The compromise of the authentication mechanism,
		which allows typically unauthenticated entities the ability to gain access
		as an authenticated user.
		\item Authorization -- The compromise of authorization information, thus
		allowing privilege escalation.
	\end{itemize}
	
	\subsection{NoSQL Injection}
	
	NoSQL (Not Only SQL) refers to the modern set of scalable, non-relational databases that have
	become intensely popular in recent years. 
	Although these databases can meet the demands of modern web applications, they
	were not designed with security in mind~\cite{6861323,6120863}.
	This has allowed NoSQL to be vulnerable to a number of exploits.
	More specifically, we will talk about MongoDB's vulnerabilities to injection.
	
	MongoDB is NoSQL database based around JSON-like files, called BSON files, 
	and horizontal scalability. 
	A database will have a set of collections. 
	Inside these collections are documents, the BSON files. 
	Each document has a set of fields which comply to the data we are storing.
	The key takeaway is that these files are schema-less.
	To execute CRUD operations on MongoDB, similar techniques found in traditional,
	relational databases are used. 
	That is, user input is taken, concatenated, and executed. 
	It is from here that MongoDB opens itself to code injections.
	Additionally, Javascript can be run directly on MongoDB, making injections
	especially disastrous~\cite{mongodb:architecture_guide}.
	
	To date several examples of injection have been found for MongoDB.
	For example, given a PHP back-end, it is possible for a poorly coded
	web application to allow tautology, union query, piggy-backed, and 
	Javascript injection to occur. 
	Furthermore, a common feature of NoSQL databases to allow querying exposes an
	HTTP REST API. 
	This can allow a cross-origin exploit~\cite{7448357}.
	Other issues in MongoDB and NoSQL security are discussed in~\cite{7275699,mongodb:faq:injection,mongodb:security_architecture,petkod.petkov2014a,petkod.petkov2014b,php:security:request_injection,php:security:script_injection,phil2010,phil2011,felipearagon2012,erlendoftedal2010,adobe:sullivan:nosql_less_security,adobe:sullivan:server-side_js}.
	
	\printsubbibliography
	
\end{document}