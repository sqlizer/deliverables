# SQLizers' deliverables

This has the deliverables (and some other stuff) for SQLizers' EEL5807 project.
Deliverables are written in LaTeX and compiled to PDF (though not always).

List of deliverables:

1. Project proposal
2. Term paper

# Useful tools

* [JabRef](http://www.jabref.org/) -- citation manager
* [Tables Generator](http://www.tablesgenerator.com/) 
  -- sane tables generation
