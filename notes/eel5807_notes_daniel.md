# Ethical Hacking Notes

## Lecture 2 -- 06.29

### Python 1

* do the "try it out" sections in the CSP book (or was it Python book?)

## Lecture 3 -- 07.01

### General Stuff

* One page proposal probably due next Wednesday (July 6).

### Python 2 

* **List / Set Comprehension** section will be on exam. (slides 33 to whatever)

* **Things To Do** sections in Python book prob exam materials.

* OOP stuff not too important for this class. Focus on scripts.
	+ Suggests to use named tuples vs OOP.

### Sockets (General A)

* ALOHAnet. Some Xerox book from 2000.

* little-endian vs big-endian

## Lecture 5 -- 07/11m

### General Stuff

* Wed is Black Hat Python. Fri is Grey Hat Python.
* Today
	- Sockets (I think)
	- Some 0-day attacks. Ref to Iran. 
* Some percentage of final can be done beforehand.

### Zero Days documentary

* Stuxnet malware.

## Lecture 6 -- 07/13w

### General Stuff

* Going to start teaching out of (1) Black Hat, and (2) Gray Hat Python
* Homeworks: (1) hacking wpa. (2) some server thing with python.
* Five page cheat sheet front and back for final exam.
* **Extra Credit:** Improve or fix any code in BHP.

### Threads

* Drawn and slides.
* C style code.
* Implementation of postincrement in assembly.
* Behavior of this function with multiple threads:
```
	void fcount() {
		for (...)
			count = count + 1;
	}
```

* Locking threads. Sleep.

### Sockets

* Five steps: create socket, bind, listen, accept, then at some point close.
* TCP and UDP client code.
* TCP server code on slides fails to close.
* Proxy server code. `~/HckCourse/MyProject2/main.py`
* This ends Chapter 2 of BHP.

## Lecture 7 -- 7/15f

### General Stuff

* **HW:** video due next Friday (7/22)
* Reading for the weekend. *Handout 1: Networking*
	- *Set C: Networking* has slides. 2 & 3 (maybe)
* Next class: scappy on Mon

### Slides: BHP2

NOTE: Try to run the code mentioned in class.

* `sniffer_ip_header_decode.py`
* `Proxyserver2.py`
* `sniffer_with_icmp.py`
	- Error with class IP
	- `c_ulong` to `c_unint32`
* Unofficial homework *Let's look at Scanner*
	- `scanner.py`
* Chs 3 & 4 of BHP

### CSPC Ch 4: Sniffers

* Play with tcpdump. 

## Lecture 8 -- 7/18m

### General Stuff

* Next class, Wed, will start to give **final exam questions** randomly.
* All hw's are group homeworks.

### Slides: BHP 3 (Scapy)

* Scapy is Ch 4 in BHP
* Scapy on HW and final exam.
* Interactive tutorial on site.

### Homework

**Due:** Fri, August 5

* Pretend *-something-* computer.

1. Define pseudo-protocol
```
	server 
		H2O // idk
	client
		REQ START SCAPPY 1000
```

2. Sockets client / server

3. Scappy $\rightarrow$ sniff. Need to do some sniffing.

4. Get info from infected computer. This includes:
	a. network interfaces
	b. filesystem / file directory / file structure
	c. whatever else

5. You need to send data back to client, ideally in file transfer.

#### Extra Credit

1. Runs in python 3

2. server is executable 

3. stealth mode (needs 2?)

4. First place (+++), second place (++), third place (+).

5. turn in by Sunday, July 31st

#### Groups of 3

* make it executable
* get hardware info

## Lecture 9 -- 7/20w

### General Stuff

* **Exam Question:**
	How do you write a small client / server using UDP in Python?

## Lecture 10 -- 7/22f

### General Stuff

* ***Extra Credit:*** Make BHP and GHP Python3-compatible.
* Sunday: 
	- Practice exam.
	- HW2 instructions
	- Create a bucket for extra credit. 
	  Ideally, just create a zip file with all files.
	- Post one more extra credit.
* Can give draft so he can check before due.

* Not all of Set C is on the exam.
	- TCP / IP is one exam.
	- Handout yes. Mainly stuff about Socket, app layer.

### CSP -- CH8: Session Hijacking / Slides

#### Written out

* PC, Router, Printer
* PC and Printer connected to Router
* Router to internet.
* IPs:
	- PC: 192.168.0.5
	- Router: 192.168.0.254
	- Printer: 192.168.100.1
* Some shit about attacking router.

### CSP -- CH8: Hacking Network Devices

* Firewalls not really on exam.

### Scapy video (CSRF group)

## Lecture 11 -- 7/27w

### General Stuff

* Handout is complementary to TCP/IP stuff.
	- Questions not coming out of the handout.

### Slides: CSPT -- Ch. 11 // DoS

* Found in Set C

### Slides: CSP -- Ch. 12 // Buffer Overflows

* Found in Set D

### HW

* Connected by TCP. 
* Need to message each other (client/server).
* To make it self-contained:
	- `py2exe` to make it to an executable.
	- Or `csfreeze`
* Then IExpress. To make it silent(?)

## Lecture 12 -- 7/29f

`future`

